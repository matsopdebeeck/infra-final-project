# Proximity Detection System

This readme file provides an overview of the Proximity Detection System, which utilizes an ultrasound range sensor, a hex (7-segment) display, LED lights, a buzzer, buttons, and a potentiometer for configuration. This system is designed to detect the presence of individuals entering through a door and provide real-time feedback on their proximity.

## Components

### 1. Ultrasound Range Sensor
The system incorporates an ultrasound range sensor to detect the distance between the sensor and the person entering through the door. This sensor provides accurate proximity data by measuring the time it takes for the ultrasonic waves to travel to the person and bounce back to the sensor.

### 2. Hex (7-Segment) Display
To provide real-time feedback, a hex (7-segment) display is integrated into the system. The display showcases the distance in centimeters, allowing easy monitoring of the proximity of the person. The display uses seven segments to represent digits, letters, or symbols.

### 3. LED Lights
The system features four LED lights that indicate the relative distance of the person from the sensor. LED 1 represents a far distance, while LED 4 represents a close distance. The intermediate LEDs (2 and 3) provide additional proximity context. The LEDs are used to visually convey the proximity information.

### 4. Buzzer
When someone enters through the door, the system triggers a buzzer to generate an audible sound. This serves as an alert to notify others in the vicinity that someone has entered. The buzzer helps provide an additional means of awareness.

### 5. Button Functionality
The system includes three buttons with different functions:
- Start Session Button: Initiates the monitoring session, activating the sensor and enabling the detection system.
- Config Menu Button: Grants access to the configuration menu, allowing adjustments to specific settings.
- Stop Session Button: Halts the monitoring session and deactivates the system.

These buttons provide control and interaction with the system, allowing users to start, configure, and stop the monitoring process.

### 6. Potentiometer for Configuration
The system utilizes a potentiometer within the configuration menu. The potentiometer allows users to adjust the distance at which the sound alert should be activated. This adjustable setting ensures flexibility according to specific requirements or preferences.

The potentiometer provides a simple and intuitive means of configuring the system to adapt to different environments.

### 7. Dynamically Allocated Log
The system maintains a log of the time and distance of each detected entry. The log is dynamically allocated using structs to store the relevant information. This log provides a historical record of the entries, allowing for analysis or review of the data.

The dynamically allocated log allows for efficient memory usage while preserving the necessary information for future reference.

## Detailed explanation

The Proximity Detection System is designed to detect the presence of individuals when they enter through a door and provide real-time feedback on their proximity. It utilizes an ultrasound range sensor called the HC-SR04 to measure the distance between the sensor and the person.

The HC-SR04 sensor works by emitting ultrasonic waves, which are sound waves with a frequency higher than what humans can hear. These waves travel from the sensor towards the person and bounce back when they encounter an obstacle, in this case, the person. The sensor then measures the time it takes for the waves to travel to the person and return to the sensor.

Using this time measurement, the system calculates the distance between the sensor and the person. It does this by utilizing the speed of sound, which is a known constant. By multiplying the time it took for the waves to travel by the speed of sound and dividing it by two (since the waves travel the distance to the person and back), the system can accurately determine the distance.

Once the distance is measured, the system checks whether the distance is less than the configured distance. The configured distance is the threshold set by the potentiometer in the configuration menu. This threshold can be adjusted based on specific requirements or preferences.

If the measured distance is less than the configured distance, it means that the person is too close to the sensor. In such a case, the system activates an alarm, which is a buzzer that generates an audible sound. This serves as an alert to notify others in the vicinity that someone has entered through the door.

Additionally, to provide visual feedback, the system uses a hex (7-segment) display and LED lights. The hex display shows the distance in centimeters, allowing for easy monitoring of the proximity of the person. The LED lights, which include four LEDs, indicate the relative distance of the person from the sensor. LED 1 represents a far distance, LED 4 represents a close distance, and LEDs 2 and 3 provide intermediate proximity context.

The system also incorporates three buttons with different functions. The Start Session Button is used to initiate the monitoring session, activating the sensor and enabling the detection system. The Config Menu Button grants access to the configuration menu, allowing adjustments to the distance threshold. The Stop Session Button is used to halt the monitoring session and deactivate the system.

Lastly, the system maintains a dynamically allocated log using structs, which records the time and distance of each detected entry. This log serves as a historical record of the entries and allows for analysis or review of the data.

Overall, the Proximity Detection System utilizes the HC-SR04 ultrasonic distance sensor to measure the distance between the sensor and a person entering through a door. It checks whether the distance is less than the configured distance, triggers an alarm if necessary, provides visual feedback through the hex display and LED lights, and allows for configuration and logging of the proximity data.
