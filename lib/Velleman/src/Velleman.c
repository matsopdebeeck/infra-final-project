/**
 * @author Mats Op de beeck @date 25/05/2023
 */
#include "Velleman.h"

/**
 * @brief Returns the data direction register (DDR) corresponding to the specified pin.
 *
 * This function calculates the data direction register (DDR) associated with the given pin and returns a
 * pointer to the volatile DDR register. The pins are grouped into ports, and the DDR controls whether each
 * pin in a port is configured as an input or an output. The function supports pins from 0 to 19, where pins
 * 0-7 correspond to Port D, pins 8-13 correspond to Port B, and pins 14-19 correspond to Port C.
 *
 * @param pin The pin number for which to retrieve the DDR, specified as a uint8_t value.
 * @return A pointer to the volatile DDR register corresponding to the specified pin.
 */
volatile uint8_t *getDataDirectionRegister(uint8_t pin) {
    return pin < 8 ? &DDRD : (pin - 8) / 6 < 1 ? &DDRB : &DDRC;
}

/**
 * @brief Returns the port register (PORT) corresponding to the specified pin.
 *
 * This function calculates the port register (PORT) associated with the given pin and returns a
 * pointer to the volatile PORT register. The pins are grouped into ports, and the PORT register controls
 * the output state of each pin in a port. The function supports pins from 0 to 19, where pins
 * 0-7 correspond to Port D, pins 8-13 correspond to Port B, and pins 14-19 correspond to Port C.
 *
 * @param pin The pin number for which to retrieve the PORT, specified as a uint8_t value.
 * @return A pointer to the volatile PORT register corresponding to the specified pin.
 */
volatile uint8_t *getPortRegister(uint8_t pin) {
    return pin < 8 ? &DDRD : (pin - 8) / 6 < 1 ? &DDRB : &DDRC;
}

/**
 * @brief Returns the pin register (PIN) corresponding to the specified pin.
 *
 * This function calculates the pin register (PIN) associated with the given pin and returns a
 * pointer to the volatile PIN register. The pins are grouped into ports, and the PIN register reads
 * the state of each pin in a port. The function supports pins from 0 to 19, where pins
 * 0-7 correspond to Port D, pins 8-13 correspond to Port B, and pins 14-19 correspond to Port C.
 *
 * @param pin The pin number for which to retrieve the PIN, specified as a uint8_t value.
 * @return A pointer to the volatile PIN register corresponding to the specified pin.
 */
volatile uint8_t *getPinRegister(uint8_t pin) {
    return pin < 8 ? &PIND : (pin - 8) / 6 < 1 ? &PINB : &PINC;
}

/**
 * @brief Returns the Pin Change Mask register (PCMSK) corresponding to the specified pin.
 *
 * This function calculates the Pin Change Mask register (PCSMK) associated with the given pin and returns a
 * pointer to the volatile PCMSK register. The pins are grouped into ports, and the PCMSK register is used to
 * enable or disable pin change interrupts on specific pins. Pin change interrupts allow the microcontroller to
 * respond to changes in the logic level of the pins, triggering an interrupt service routine (ISR) when a
 * specified pin changes its state. The function supports pins from 0 to 19, where pins
 * 0-7 correspond to Port D, pins 8-13 correspond to Port B, and pins 14-19 correspond to Port C.
 *
 * @param pin The pin number for which to retrieve the PCMSK, specified as a uint8_t value.
 * @return A pointer to the volatile PCMSK register corresponding to the specified pin.
 */
volatile uint8_t *getPinChangeMaskRegister(uint8_t pin) {
    return pin < 8 ? &PCMSK2 : (pin - 8) / 6 < 1 ? &PCMSK0 : &PCMSK1;
}

/**
 * @brief Retrieves the Pin Change Interrupt group for the specified digital pin.
 *
 * This function determines the Pin Change Interrupt group associated with the specified digital pin.
 * The Pin Change Interrupt groups divide the pins into different sets based on their interrupt capabilities.
 *
 * @param pin The digital pin number to retrieve the interrupt group for.
 * @return The Pin Change Interrupt group number for the specified pin, ranging from 0 to 2.
 *         Returns -1 if the pin does not support pin change interrupts.
 */
uint8_t getPinChangeInterruptGroup(uint8_t pin) {
    return pin < 8 ? 2 : (pin - 8) / 6 < 1 ? 0 : 1;
}

/**
 * @brief Gets the corresponding port pin number for a given pin number.
 *
 * This function maps a given pin number to its corresponding port pin number. In AVR microcontrollers,
 * the port pin numbers for digital pins are typically arranged sequentially, starting from 0. However,
 * for certain microcontrollers, there may be variations in pin numbering schemes or grouping of pins.
 *
 * The function takes a pin number as input and returns the corresponding port pin number.
 * If the pin number is less than 8, it is directly returned as the port pin number.
 * For pin numbers greater than or equal to 8, the function calculates the corresponding port pin number
 * based on the pin's relationship to the initial digital pin number.
 *
 * @param pin The pin number for which the corresponding port pin number is to be obtained.
 * @return The corresponding port pin number as a uint8_t value.
 */
uint8_t getPortPin(uint8_t pin) {
    return pin < 8 ? pin : (pin - 8) < 6 ? (pin - 8) : pin - 14;
}

/**
 * @brief Configures the mode (input/output) for the specified pin.
 *
 * This function sets the mode (input/output) for the specified pin by modifying the corresponding data direction register (DDR).
 * The pin number and mode are provided as parameters. The function supports pins from 0 to 19, where pins 0-7 correspond to Port D,
 * pins 8-13 correspond to Port B, and pins 14-19 correspond to Port C.
 *
 * @param pin The pin number for which to configure the mode, specified as a uint8_t value.
 * @param mode The mode to set for the pin, specified as a PinMode enumeration value (INPUT or OUTPUT).
 */
void pinMode(uint8_t pin, enum PinMode mode) {
    volatile uint8_t *ddr = getDataDirectionRegister(pin);
    volatile uint8_t *port = getPortRegister(pin);

    if (mode == OUTPUT) {
        *ddr |= (1 << getPortPin(pin));
        *port &= ~(1 << getPortPin(pin));
    } else if (mode == INPUT) {
        *ddr &= ~(1 << getPortPin(pin));
        *port &= ~(1 << getPortPin(pin));
    }
}

/**
 * @brief Writes a digital value (HIGH or LOW) to the specified pin.
 *
 * This function writes a digital value (HIGH or LOW) to the specified pin by modifying the corresponding port register (PORT).
 * The pin number and value are provided as parameters. The function supports pins from 0 to 19, where pins 0-7 correspond to Port D,
 * pins 8-13 correspond to Port B, and pins 14-19 correspond to Port C.
 *
 * @param pin The pin number to write the value to, specified as a uint8_t value.
 * @param value The digital value to write (HIGH or LOW), specified as a State enumeration value.
 */
void digitalWrite(uint8_t pin, enum State value) {
    volatile uint8_t *port = getPortRegister(pin);

    if (value == HIGH) {
        *port |= (1 << getPortPin(pin));
    } else if (value == LOW) {
        *port &= ~(1 << getPortPin(pin));
    }

}

/**
 * @brief Reads the digital value (HIGH or LOW) from the specified pin.
 *
 * This function reads the digital value (HIGH or LOW) from the specified pin by accessing the corresponding pin register (PIN).
 * The pin number is provided as a parameter. The function supports pins from 0 to 19, where pins 0-7 correspond to Port D,
 * pins 8-13 correspond to Port B, and pins 14-19 correspond to Port C.
 *
 * @param pin The pin number to read the value from, specified as a uint8_t value.
 * @return The digital value read from the pin (HIGH or LOW), as a State enumeration value.
 */
enum State digitalRead(uint8_t pin) {
    volatile uint8_t *pinReg = getPinRegister(pin);
    return (*pinReg & (1 << getPortPin(pin))) != 0 ? LOW : HIGH;
}

/**
 * @brief Reads analog voltage on the specified pin and returns the corresponding digital value.
 *
 * This function reads the analog voltage on the specified pin and returns the corresponding digital value.
 * The pin number is provided as a parameter. The function supports pins from 0 to 5, corresponding to the analog input pins.
 * It configures the ADC to read the analog voltage on the specified pin and waits for the conversion to complete.
 * Once the conversion is complete, it returns the 10-bit digital value of the analog voltage.
 *
 * @param pin The analog input pin to read, specified as a uint8_t value.
 * @return The 10-bit digital value of the analog voltage read from the specified pin, as a uint16_t value.
 */
uint16_t analogRead(uint8_t pin) {
    if (pin >= A0 && pin <= A5) {
        pin -= A0;
        ADMUX = (ADMUX & 0xF0) | (pin & 0x0F);
        ADMUX |= ( 1 << REFS0 );
        ADCSRA |= ( 1 << ADPS2 ) | ( 1 << ADPS1 ) | ( 1 << ADPS0 );
        ADCSRA |= ( 1 << ADEN );
        ADCSRA |= ( 1 << ADSC );
        loop_until_bit_is_clear( ADCSRA, ADSC );
        return ADC;
    } else {
        return 0;
    }
}

/**
 * @brief Writes an analog value (0-1023) to the specified analog pin using PWM.
 *
 * This function writes an analog value (0-1023) to the specified analog pin using pulse-width modulation (PWM).
 * The pin number and value are provided as parameters. The function configures the corresponding timer registers
 * to generate PWM signal with the desired duty cycle.
 *
 * @param pin The analog pin number to write the analog value to, specified as a uint8_t value (A0 to A5).
 * @param value The analog value to write (0-1023), specified as a uint16_t value.
 */
void analogWrite(uint8_t pin, uint16_t value) {
    if (pin >= A0 && pin <= A5) {
        pin -= A0;
        switch (pin) {
            case 0:  // A0, mapped to digital pin 14
                TCCR1A |= (1 << COM1A1) | (1 << WGM10);
                TCCR1B |= (1 << WGM12) | (1 << CS11);
                OCR1A = value * 4;
                break;
            case 1:  // A1, mapped to digital pin 15
                TCCR1A |= (1 << COM1B1) | (1 << WGM10);
                TCCR1B |= (1 << WGM12) | (1 << CS11);
                OCR1B = value * 4;
                break;
            case 2:  // A2, mapped to digital pin 16
                TCCR2A |= (1 << COM2A1) | (1 << WGM20) | (1 << WGM21);
                TCCR2B |= (1 << CS21);
                OCR2A = value * 4;
                break;
            case 3:  // A3, mapped to digital pin 17
                TCCR0A |= (1 << COM0B1) | (1 << WGM00) | (1 << WGM01);
                TCCR0B |= (1 << CS01);
                OCR0B = value * 4;
                break;
            case 4:  // A4, mapped to digital pin 18
                TCCR2A |= (1 << COM2B1) | (1 << WGM20) | (1 << WGM21);
                TCCR2B |= (1 << CS21);
                OCR2B = value * 4;
                break;
            case 5:  // A5, mapped to digital pin 19
                TCCR0A |= (1 << COM0A1) | (1 << WGM00) | (1 << WGM01);
                TCCR0B |= (1 << CS01);
                OCR0A = value * 4;
                break;
            default:
                break;
        }
    }
}

/**
 * @brief Interrupt Service Routines (ISRs) for Pin Change Interrupts.
 *
 * These ISRs are executed when pin change interrupts are triggered on the corresponding pin change interrupt vectors.
 * The ISRs call the functions pointed to by the respective function pointers: `isr0`, `isr1`, and `isr2`.
 *
 * @note The function pointers (`isr0`, `isr1`, `isr2`) must be assigned to the desired ISR functions before attaching interrupts.
 *       If a function pointer is not assigned, the respective ISR will not call any function.
 */
volatile void (*isr0)();
volatile void (*isr1)();
volatile void (*isr2)();

ISR(PCINT0_vect) {
    isr0();
}
ISR(PCINT1_vect) {
    isr1();
}
ISR(PCINT2_vect) {
    isr2();
}

/**
 * @brief Attaches a pin change interrupt to the specified pin.
 *
 * This function attaches a pin change interrupt to the specified pin, enabling its interrupt functionality.
 * The pin change interrupt can be triggered by a state change on the pin, depending on the supported pin change interrupt groups.
 * The function automatically determines the pin change interrupt group based on the pin number and configures the necessary registers.
 * After attaching the interrupt, the global interrupt flag is enabled to allow the interrupt to be triggered.
 *
 * @param pin The pin number to attach the pin change interrupt to.
 *
 * @return None.
 */
void attachInterrupt(uint8_t pin, volatile void (*isr)()) {
    uint8_t PCIE = getPinChangeInterruptGroup(pin);
    volatile uint8_t* pcmskRegister = getPinChangeMaskRegister(pin);

    PCICR |= _BV(PCIE);
    *pcmskRegister |= _BV(getPortPin(pin));

    switch (PCIE) {
        case 0:
            isr2 = isr;
            break;
        case 1:
            isr1 = isr;
            break;
        case 2:
            isr0 = isr;
            break;
        default:
            break;
    }

    sei();
}

/**
 * @brief Detaches the pin change interrupt from the specified pin.
 *
 * This function detaches the pin change interrupt from the specified pin, disabling its interrupt functionality.
 * The function automatically determines the pin change interrupt group based on the pin number and clears the necessary registers.
 * After detaching the interrupt, the global interrupt flag is enabled to allow other interrupts to be triggered.
 *
 * @param pin The pin number to detach the pin change interrupt from.
 *
 * @return None.
 */
void detachInterrupt(uint8_t pin) {
    uint8_t PCIE = getPinChangeInterruptGroup(pin);
    volatile uint8_t *pcmskRegister = getPinChangeMaskRegister(pin);

    *pcmskRegister &= ~_BV(getPortPin(pin));

    switch (PCIE) {
        case 0:
            isr2 = 0;
            break;
        case 1:
            isr1 = 0;
            break;
        case 2:
            isr0 = 0;
        default:
            break;
    }
}

/**
 * @brief Counts the width of a pulse on a specified pin using assembly instructions.
 *
 * This function measures the duration of a pulse on a specific pin by monitoring the state of the pin.
 * It waits for any previous pulse to end, then waits for the pulse to start,
 * and finally measures the width of the pulse. The function supports a maximum number of loops to prevent indefinite waiting.
 *
 * @param port Pointer to the port register of the pin to monitor.
 * @param bit Bit number of the pin to monitor.
 * @param stateMask Desired state of the pin (HIGH or LOW) for monitoring the pulse.
 * @param maxloops Maximum number of loops to wait for the pulse.
 * @return The width of the pulse in microseconds. Returns 0 if the pulse is not detected within the specified maximum number of loops.
 */
unsigned long countPulse(const volatile uint8_t* port, uint8_t bit, uint8_t stateMask, unsigned long maxloops) {
    unsigned long width = 0;

    while ((*port & bit) == stateMask) {
        if (--maxloops == 0) {
            return 0;
        }
    }
    while ((*port & bit) != stateMask) {
        if (--maxloops == 0) {
            return 0;
        }
    }
    while ((*port & bit) == stateMask) {
        if (++width == maxloops) {
            return 0;
        }
    }

    return width;
}

/**
 * @brief Measures the duration of a pulse on a specified pin.
 *
 * This function measures the duration of a pulse on a specified pin using the `countPulse` function.
 * It monitors the state of the pin and calculates the width of the pulse based on the given parameters.
 *
 * @param pin The pin number to monitor the pulse.
 * @param state The state (HIGH or LOW) to monitor for the pulse.
 * @param timeout The maximum time to wait for the pulse, in microseconds.
 * @return The width of the pulse in microseconds. Returns 0 if the pulse is not detected within the specified timeout period.
 */
unsigned long pulseIn(uint8_t pin, enum State state, unsigned long timeout) {
    const volatile uint8_t* pinRegister = getPinRegister(pin);
    uint8_t bit = _BV(getPortPin(pin));
    uint8_t stateMask = (state ? bit : 0);

    unsigned long maxLoops = ((timeout) * (F_CPU / 1000000L)) / 16;
    unsigned long width = countPulse(pinRegister, bit, stateMask, maxLoops);

    if (width)
        return ((width * 16 + 16) / (F_CPU / 1000000L));
    else
        return 0;
}

/**
 * @brief Plays a note on the specified pin.
 *
 * This function generates a square wave of the specified frequency and duration on the given pin.
 * The pin should be configured as an output using the `pinMode` function before calling this function.
 *
 * @param pin The pin number to play the note on.
 * @param freq The frequency of the note in Hz.
 * @param time The duration of the note in milliseconds.
 *
 * @return None.
 */
void playNote(uint8_t pin, double freq, uint32_t time) {
    volatile uint8_t *port = getPortRegister(pin);

    int delay = (int) (1.0 / freq * 500000.0);
    for (uint32_t t = 0; t < time; t += 2 * delay) {
        *port |= _BV(getPortPin(pin));
        _delay_us(delay);
        *port &= ~_BV(getPortPin(pin));
        _delay_us(delay); }
}