/**
* @author Mats Op de Beeck @date 25/05/2023
*/

#ifndef VELLEMAN_H
#define VELLEMAN_H

#include <avr/io.h>
#include <avr/interrupt.h>
#define __DELAY_BACKWARD_COMPATIBLE__
#include <util/delay.h>

#define A0 14
#define A1 15
#define A2 16
#define A3 17
#define A4 18
#define A5 19

/**
 * @brief Enumeration for pin mode (input/output).
 */
enum PinMode {
    INPUT,   /**< Input mode */
    OUTPUT   /**< Output mode */
};

/**
 * @brief Enumeration for pin state (low/high).
 */
enum State {
    LOW,   /**< Low state */
    HIGH   /**< High state */
};

void pinMode(uint8_t pin, enum PinMode mode);
void digitalWrite(uint8_t pin, enum State value);
enum State digitalRead(uint8_t pin);
void analogWrite(uint8_t pin, uint16_t value);
uint16_t analogRead(uint8_t pin);
void attachInterrupt(uint8_t pin, volatile void (*isr)());
void detachInterrupt(uint8_t pin);
unsigned long pulseIn(uint8_t pin, enum State state, unsigned long timeout);
void playNote(uint8_t pin, double freq, uint32_t time);

#endif