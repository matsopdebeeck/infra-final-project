#include "HC-SR04.h"

uint8_t trigPin;
uint8_t echPin;

/**
 * @brief Initializes the ultrasonic sensor.
 *
 * This function sets up the necessary configurations for the ultrasonic sensor to work correctly.
 * It initializes the timer and sets the trigger and echo pins.
 *
 * @param triggerPin The pin connected to the trigger pin of the ultrasonic sensor.
 * @param echoPin The pin connected to the echo pin of the ultrasonic sensor.
 */
void initUltrasonic(uint8_t triggerPin, uint8_t echoPin) {
    initUSART();

    trigPin = triggerPin;
    echPin = echoPin;

    pinMode(trigPin, OUTPUT);
    pinMode(echPin, INPUT);
}

/**
 * @brief Measures the distance in centimeters using an ultrasonic sensor.
 *
 * This function calculates the distance by sending a pulse and measuring the time it takes for
 * the echo to return. It uses the speed of sound in air to convert the time into distance.
 *
 * @return The measured distance in centimeters.
 *
 * @note Due to rounding errors, an additional adjustment is made when returning the distance
 */
unsigned long measureDistanceCm() {
    double speedOfSoundInCmPerMicroSec = 0.0345;
    digitalWrite(trigPin, LOW);
    _delay_us(2);
    digitalWrite(trigPin, HIGH);
    _delay_us(10);
    digitalWrite(trigPin, LOW);

    unsigned long maxDistanceDurationMicroSec = 2.5 * maxDistanceCm / speedOfSoundInCmPerMicroSec; // 25% timeout margin
    unsigned long durationMicroSec = pulseIn(echPin, HIGH, maxDistanceDurationMicroSec);

    unsigned long distanceCm = durationMicroSec / 2 * speedOfSoundInCmPerMicroSec;
    if (distanceCm > 4 && distanceCm < maxDistanceCm) {
        return distanceCm + 2;
    } else {
        return 0;
    }
}