/**
 * @author Mats Op de beeck @date 31/05/2023
 */

#ifndef ULTRASONIC_DISTANCE_SENSOR_H
#define ULTRASONIC_DISTANCE_SENSOR_H

#include <avr/io.h>
#include <avr/interrupt.h>
#include <Velleman.h>
#include <usart.h>

#define maxDistanceCm 400

void initUltrasonic(uint8_t triggerPin, uint8_t echoPin);
unsigned long measureDistanceCm();

#endif