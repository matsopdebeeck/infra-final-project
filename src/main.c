#include <usart.h>
#include <display.h>
#include <HC-SR04.h>
#include <assert.h>

volatile uint8_t sessionStart = 0;
volatile uint8_t sessionStop = 0;
volatile unsigned long configuredDistance = 100;

volatile uint32_t seconds = 0;

typedef struct LogEntry {
    uint8_t logEntryID;
    uint8_t seconds;
    uint8_t minutes;
    uint8_t hours;
    uint16_t distance;
} LogEntry;

void initTimer() {
    seconds = 0;
    TCCR1B = (1 << WGM12) | (1 << CS12) | (1 << CS10);
    OCR1A = 15624;
    TIMSK1 = (1 << OCIE1A);

    sei();
}

ISR(TIMER1_COMPA_vect) {
    seconds++;
}

void buttonInterrupts() {
    _delay_us(100); // debounce
    if (digitalRead(15) == HIGH) {
        sessionStart = 1;
        initTimer();
    } else if (digitalRead(16) == HIGH) {
         uint8_t loading[] = {~0x80, ~0x80, ~0x80, ~0x80};
         writeCustomAndWait(loading, 1000);
        while (digitalRead(16) == LOW) {
            for (int i = 0; i < 4; ++i) {
                configuredDistance += analogRead(A0);
            }
            configuredDistance = configuredDistance / 5.115;
            writeNumberAndWait(configuredDistance, 100);
        }
        writeCustomAndWait(loading, 500);
    } else if (digitalRead(17) == HIGH) {
        sessionStop = 1;
    }
}

int main() {
    // Set up pins for input and output
    pinMode(A0, INPUT); // Potentiometer

    pinMode(15,INPUT); // Button 1
    pinMode(16, INPUT); // Button 2
    pinMode(17, INPUT); // Button 3

    pinMode(10, OUTPUT); // LED D4
    pinMode(11, OUTPUT); // LED D3
    pinMode(12, OUTPUT); // LED D2
    pinMode(13, OUTPUT); // LED D1

    pinMode(3, OUTPUT); // Buzzer

    // Attach necessary interrupts
    attachInterrupt(15, (volatile void (*)()) &buttonInterrupts); // Attach interrupt to button 1
    attachInterrupt(16, (volatile void (*)()) &buttonInterrupts); // Attach interrupt to button 2
    attachInterrupt(17, (volatile void (*)()) &buttonInterrupts); // Attach interrupt to button 3

    // Initialize necessary libraries
    initDisplay();
    initUSART();
    initUltrasonic(6, 5); // Set up ultrasonic sensor

    while (1) {
        // Set up dynamic memory allocation for log
        int length = 10;
        LogEntry* pLog = (LogEntry*) calloc(length, sizeof(LogEntry)); // Allocate memory for 10 LogEntries
        if (pLog == 0) {
            printf("Couldn't allocate memory");
            exit(0);
        }
        int currPos = 0;
        uint8_t dashes[] = {~0x40, ~0x40, ~0x40, ~0x40};
        clearDisplay();
        _delay_ms(500);
        writeCustomAndWait(dashes, 500);
        while (sessionStart == 1 && sessionStop == 0) {
            uint16_t distAvg = measureDistanceCm();
            for (int i = 0; i < 4; ++i) {
                distAvg += measureDistanceCm();
                distAvg /= 2;
            }
            digitalWrite(10, LOW);
            digitalWrite(11, LOW);
            digitalWrite(12, LOW);
            digitalWrite(13, LOW);
            if (distAvg < 100) {
                digitalWrite(10, HIGH);
            } else if (distAvg < 200) {
                digitalWrite(11, HIGH);
            } else if (distAvg < 300) {
                digitalWrite(12, HIGH);
            } else {
                digitalWrite(13, HIGH);
            }
            if (distAvg < configuredDistance) {
                if (currPos < length) {
                    uint8_t hours = (seconds / 3600);
                    uint8_t minutes = (seconds % 3600) / 60;
                    uint8_t secs = seconds % 60;
                    LogEntry logEntry = {currPos, secs, minutes, hours, distAvg};
                    pLog[currPos++] = logEntry;
                } else {
                    if (currPos > 39) {
                        length = 10;
                        currPos = 0;
                        pLog = realloc(pLog, length * sizeof(LogEntry));
                        uint8_t hours = (seconds / 3600);
                        uint8_t minutes = (seconds % 3600) / 60;
                        uint8_t secs = seconds % 60;
                        LogEntry logEntry = {currPos, secs, minutes, hours, distAvg};
                        pLog[currPos++] = logEntry;
                    } else {
                        length += 1;
                        pLog = realloc(pLog, length * sizeof(LogEntry));
                        uint8_t hours = (seconds / 3600);
                        uint8_t minutes = (seconds % 3600) / 60;
                        uint8_t secs = seconds % 60;
                        LogEntry logEntry = {currPos, secs, minutes, hours, distAvg};
                        pLog[currPos++] = logEntry;
                    }
                }
                // playNote works with microseconds
                playNote(3, 256.8912, 125000);
                playNote(3, 323.6672, 125000);
                playNote(3, 384.944, 125000);
                writeNumberAndWait(distAvg, 1000);
            } else {
                writeNumberAndWait(distAvg, 125);
            }
            clearDisplay();

        }
        if (sessionStart == 1 && sessionStop == 1) {
            clearDisplay();
            digitalWrite(10, LOW);
            digitalWrite(11, LOW);
            digitalWrite(12, LOW);
            digitalWrite(13, LOW);
            printf("Printing log...\n");
            for (int i = 0; i < currPos; ++i) {
                printf("Entry %d.\n  "
                       "The configured distance was exceeded after %d hour(s), "
                       "%d minute(s) and %d seconds\n  "
                       "Distance from sensor was: %d cm\n\n",
                       pLog[i].logEntryID + 1,
                       pLog[i].hours,
                       pLog[i].minutes,
                       pLog[i].seconds,
                       pLog[i].distance);
            }
            configuredDistance = 100;
            sessionStart = 0;
            sessionStop = 0;
        }
        free(pLog);
    }
}